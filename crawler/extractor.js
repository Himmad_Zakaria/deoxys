var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var exec = require('child-process-promise').exec;
var colors = require('colors');
// var url = require('url');
// var sleep = require('sleep');

// connect to DB
mongoose.connect('mongodb://localhost/crawler');
var db = mongoose.connection;
db.on('error', console.error.bind(console, '[✖] connection error:'));
db.once('open', function callback () {
  console.log(colors.green("[✔] connected to database"));
});

// setup schemas
var linkSchema = new Schema({
    url:  String,
    lastmod:Date,
    lastchk:Date,
    crawled:Boolean
});

var movieSchema = new Schema({
    title: String,
    links: Array
});

var link = mongoose.model('links', linkSchema);
var movie = mongoose.model('movies',movieSchema);
var CID = null; // Current  ID of a link
var addedCount = 0;
var totalLinks = 0;
var processedLinks = 0;

// find the number of links we need to process
link.count({crawled:false},function(err,count){
    totalLinks = count;
    console.log(colors.yellow("[ℹ] number of unprocessed links is: " + count));
});


function initMe (){
    // select random link to allow multi workers 
    var rand = Math.floor(Math.random() * totalLinks); 
    link.findOne({crawled:false}).skip(rand)
        .exec(function(err,doc){
            if(err) console.log(err);
            console.log('[⚡] Processing now: ' + doc.url);
            processedLinks++;
            CID = doc._id;

            exec('casperjs streamcomplt.js ' + doc.url)
                .then(function (result) {
                    var stdout = result.stdout;
                    var stderr = result.stderr;
                    // parse stdout
                    var data = parseResult(stdout);
                    if (data){
                    console.log("[ℹ] title: " + data.title + "\n    iframe: " + data.iframe);
                        var movie_ = new movie({"title":data.title,"links":data.iframe});
                        movie.findOne({title:data.title}).exec(function(err,doc){
                            if (err) console.log(colors.red("[✖] Can't check find if movie allready exisit"));
                            if (!doc){
                                save2db(movie_,CID);
                            } else {
                                console.log(colors.yellow.underline("[⚠] Movie allready exisit"));
                                updateStat(CID);
                            }
                        });
                    } else {
                        console.log(colors.yellow.underline("[⚠] return results empty, going to next link"));
                        updateStat(CID);
                    }

                })
                .fail(function (err) {
                    console.error('ERROR: ', err);
                })
                .progress(function (childProcess) {
                    // console.log('childProcess.pid: ', childProcess.pid);
                });
        });
}


function updateStat (id){
link.findOne({"_id":id})
    .exec(function(err,link){
        if(err) console.log(err);
        console.log(colors.cyan('[⚡] updating status to cralwed: ' + link.url));
        console.log(colors.cyan('[i] processed links ' + processedLinks +'/'+ totalLinks  ));
        link.crawled = true;
        link.lastchk = Date.now();
        link.save(function(err,doc){
            if (err) console.log(colors.red.underline("[✖] can't update status to crawled aborting!!"));
            console.log("[✔] status changed correctly passing to the next record");

            initMe();
        });
    });
}

function save2db (data,CID){
    data.save(function(err,doc){
        if (err) console.log(colors.red.underline("[✖] can't save a record to the database!!"));
        addedCount++;
        console.log(colors.inverse("[★] a new movie added to DB correctly, "+ addedCount +" movie sofare                "));
    updateStat(CID);
    });    
}

function parseResult (data) {
    data = data.trim()
    if (data != "null"){
        data = JSON.parse(data);
        return data;
    }else{    
        return null;
    }
}

 initMe();
