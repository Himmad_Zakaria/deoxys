var casper = require('casper').create();
var url = casper.cli.args[0]
casper.start(url, function() {
	var title = this.fetchText('#content > div.leftC > div:nth-child(1) > div > center > h1');
	var iframe = this.getElementAttribute('#content > div.leftC > div:nth-child(1) > div > div.filmicerik > p:nth-child(2) > iframe','src')
	if (iframe){
		this.echo('{"title":"'+ title +'","iframe":"'+ iframe +'"}');
	} else {
		this.echo('null');	
	}

});
casper.run();
