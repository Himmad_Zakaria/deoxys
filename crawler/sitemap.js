var fs = require('fs'),
    xml2js = require('xml2js'),
    colors = require('colors'),
    mongoose = require('mongoose'),
    Schema = mongoose.Schema;

// connect to DB
mongoose.connect('mongodb://localhost/crawler');
var db = mongoose.connection;
db.on('error', console.error.bind(console, '[✖] connection error:'));
db.once('open', function callback () {
  console.log(colors.green("[✔] connected to database"));
});

// setup schemas
var linkSchema = new Schema({
    url:  String,
    lastmod:Date,
    lastchk:Date,
    crawled:Boolean
});
var link = mongoose.model('links', linkSchema);
var index = 0;
var count = 0,
    countAdd=0;

// read the site map and put it in db
var parser = new xml2js.Parser();
fs.readFile('sitemap.xml', function(err, data) {
    // processing sitemap.xml
    parser.parseString(data, function (err, result) {
        if (err) console.log(colors.red("[ERROR] can't parse sitemap!: " + err));
        links = result.urlset.url;
        savelink(links,index);
    });
});

function savelink (nodes,index){
    if ((nodes.length - count) ==  0 ){
    console.log(colors.green("[OK] " + countAdd + " link was added to database"));
    console.log(colors.blue("[i] "+ nodes.length +" link checked, closing db connection now "));
    db.close();
    } else {
        var node = new link({
            "url": nodes[index].loc ,
            "lastmod":Date.parse(nodes[index].lastmod) || Date.now(),
            "lastchk":Date.now(),
            "crawled":false
        });
        link.findOne({'url':node.url},function(err,doc){
            if (err) console.log(colors.red("[✖] can't check if link allready exisit!: " + err));
            if (!doc){
                node.save(function(err,doc){
                    if (err) console.log(colors.red("[✖] can't save a record to the database!! " + err));
                        count++;
                        countAdd++;
                        console.log("-------------------------------------------------------------");
                        console.log(colors.green("[OK] new link added : " + node.url));
                        console.log(colors.yellow("[i] " + (nodes.length - count) + ' remaining links'));
                        console.log(colors.blue('[>>] '+ count +'/'+ nodes.length ));
                        savelink(nodes,count);
                });
            } else {
                count++;
                console.log(colors.yellow("[i] link allready exisit: " + node.url) );
                console.log(colors.yellow("[i] " + (nodes.length - count) + ' remaining links'));
                console.log(colors.yellow('[i] '+ count +'/'+ nodes.length) );
                savelink(nodes,count);
            }
        });
    }
}



